#!/usr/bin/env oil
var PROJECT_ROOT = $(cd "$_this_dir" { pwd })

cd $PROJECT_ROOT {
  ansible all -m ping
}
