#!/usr/bin/env oil
var PROJECT_ROOT = $(cd "$_this_dir" { pwd })

cd $PROJECT_ROOT {
  ansible-playbook playbooks/emush/main.yml --ask-become-pass --ask-vault-pass
}
