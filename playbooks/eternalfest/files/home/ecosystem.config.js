module.exports = {
  apps: [
    {
      name: "eternalfest",
      env: {
        "NODE_ENV": "production",
      },
      cwd: "/var/www/eternalfest.net/repos/eternalfest.net/packages/website",
      interpreter : 'node@14.14.0',
      script: "/var/www/eternalfest.net/repos/eternalfest.net/packages/website/main/main.js",
      watch: false
    }
  ],
};
