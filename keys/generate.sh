#!/usr/bin/env oil
var ACTIVE_KEYS_DIR = "$(cd "$(dirname "${BASH_SOURCE[0]}")/active" && pwd)"
# Generate random 8 character alphanumeric string
cd $ACTIVE_KEYS_DIR {
  var BACKUP_DATE="$(date --iso-8601)"
  var BACKUP_ID="$(head -c 256 /dev/urandom | tr -dc '$_a-zA-Z0-9' | fold -w 8 | head -n 1)"
  # -t: Type
  # -C: Comment
  # -f: Output file
  # -N: Password
  ssh-keygen -t ed25519 -C "contact@eternal-twin.net" -f "emush.${BACKUP_DATE}.${BACKUP_ID}.id_ed25519" -N ""
}

echo "Key generation complete"
